//TODO:change the implementation to use a map for localStorage

var localStorageCounterKey = "todo-counter";

var counter = initiaLizeCounter();

function TodoItem(text, key, isDone) {
    this.text = text;
    this.key = key;
    this.isDone = isDone;
    TodoItem.prototype.toString = function () {
        return "[" + this.text + "," + this.key + "," + this.isDone + "]";
    }
}

$(function () {

    loadLocalStorage();

    $("#newTask").on("keyup", function addItem(event) {
        if (event.which === 13) {
            $("ul.list-group").prepend('<li class="list-group-item">' + event.target.value +
                '<button class="close" type="button">&times;</button>' +
                "</li>");
            var item = event.target.value;
            $("#newTask").val("");
            counter = Number(counter) + 1;
            saveCounter(counter);
            var todo = new TodoItem(item, counter, false);
            savetoLocalStorage(todo);
        }
    });

    $("ul.list-group").on("click", 'li', function () {
        $(this).toggleClass("done");
        var text = $(this).text();
        text = text.substring(0, text.length - 1);
        var isDone = $(this).hasClass("done");
        var todo = new TodoItem(text, counter, isDone);
        todo.key = getLocalStorageKey(todo);
        savetoLocalStorage(todo);
    });

    $("ul.list-group").on("click", 'li .close', function () {
        $(this).parent().fadeOut('slow', function () {
            var text = $(this).text();
            text = text.substring(0, text.length - 1);
            $(this).remove();
            var todo = new TodoItem(text, counter, true);
            todo.key = getLocalStorageKey(todo);
            removeFromLocalStorage(todo);
        });
    });

});

function loadLocalStorage() {
    if (localStorage.length > 0) {
        for (var key in localStorage) {
            if (localStorage.hasOwnProperty(key)) {
                if (Number(key)) {
                    var todoObject = JSON.parse(localStorage.getItem(key));
                    var domClass = '<li class="list-group-item">';
                    if (todoObject.isDone) {
                        domClass = '<li class="list-group-item done">';
                    }
                    $("ul.list-group").prepend(domClass +
                        todoObject.text + '<button class="close" type="button">&times;</button>' +
                        "</li>");
                }
            }
        }
    }
}

function savetoLocalStorage(item) {
    localStorage.setItem(item.key, JSON.stringify(item));
}

function getLocalStorageKey(item) {
    for (var key in localStorage) {
        if (localStorage.hasOwnProperty(key)) {
            var obj = JSON.parse(localStorage.getItem(key));
            if (obj.text == item.text) {
                return obj.key;
            }
        }
    }
}

function removeFromLocalStorage(item) {
    localStorage.removeItem(item.key);
}

function initiaLizeCounter() {
    return localStorage.getItem(localStorageCounterKey) == null ? 0 : localStorage.getItem('todo-counter');
}

function saveCounter(value) {
    localStorage.setItem(localStorageCounterKey, value);
}




